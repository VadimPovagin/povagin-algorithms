using namespace cv;
using namespace std;


bool Dimm(Mat& image)
{
	Mat aux;
	Scalar mean, dev;
	vector<float> msd;
	vector<float> mean_;
	for (int y = 0; y < image.rows; y++)
	{
		aux = image.colRange(0, image.cols - 1).rowRange(y, y + 1);
		meanStdDev(aux, mean, dev);
		msd.push_back(dev.val[0]);
		mean_.push_back(mean.val[0]);
	}

	bool f = false;
	bool found = false;
	int counter = 0;
	float gradient = 0;


	vector<float> q;
	for (int i = 0; i < msd.size() - 1; i++)
	{
		q.push_back(fabs(mean_[i + 1] - mean_[i]));
	}

	int i = 0;
	while (i < msd.size() - 1)
	{
		if ((msd[i] < 10)
			&&
			((fabs(mean_[i + 1] - mean_[i]) < 12) && (mean_[i] < 120)
				||
				(fabs(mean_[i + 1] - mean_[i]) < 15) && (mean_[i] >= 120))
			)
		{
			f = true;
			counter++;
			gradient += mean_[i + 1] - mean_[i];
		}
		else
		{
			f = false;
			gradient /= counter;

			if ((counter > 22) && (counter < 50) && (fabs(gradient) < 2))
			{
				found = true;
			}
			counter = 0;
			gradient = 0;
		}
		i++;
	}
	return found;
}
#pragma once
