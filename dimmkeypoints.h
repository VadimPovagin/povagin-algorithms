#pragma once

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <cmath>

#include "contour.h"

using namespace cv;
using namespace std;

Mat dst;

const double EPS = 1E-9;

struct pt {
	double x, y;

	bool operator< (const pt & p) const {
		return x < p.x - EPS || abs(x - p.x) < EPS && y < p.y - EPS;
	}

	bool operator> (const pt & p) const {
		return !operator<(p);
	}
};

struct CEdge
{
	pt p[2];
	float k;
};

struct line_ {
	double a, b, c;

	line_() {}
	line_(pt p, pt q) {
		a = p.y - q.y;
		b = q.x - p.x;
		c = -a * p.x - b * p.y;
		norm();
	}

	void norm() {
		double z = sqrt(a*a + b * b);
		if (abs(z) > EPS)
			a /= z, b /= z, c /= z;
	}

	double dist(pt p) const {
		return a * p.x + b * p.y + c;
	}
};

#define det(a,b,c,d)  (a*d-b*c)

inline bool betw(double l, double r, double x) {
	return min(l, r) <= x + EPS && x <= max(l, r) + EPS;
}

inline bool intersect_1d(double a, double b, double c, double d) {
	if (a > b)  swap(a, b);
	if (c > d)  swap(c, d);
	return max(a, c) <= min(b, d) + EPS;
}

bool intersect(pt a, pt b, pt c, pt d, pt & left, pt & right) {
	if (!intersect_1d(a.x, b.x, c.x, d.x) || !intersect_1d(a.y, b.y, c.y, d.y))
		return false;
	line_ m(a, b);
	line_ n(c, d);
	double zn = det(m.a, m.b, n.a, n.b);
	if (abs(zn) < EPS) {
		if (abs(m.dist(c)) > EPS || abs(n.dist(a)) > EPS)
			return false;
		if (b < a)  swap(a, b);
		if (d < c)  swap(c, d);
		left = max(a, c);
		right = min(b, d);
		return true;
	}
	else {
		left.x = right.x = -det(m.c, m.b, n.c, n.b) / zn;
		left.y = right.y = -det(m.a, m.c, n.a, n.c) / zn;
		return betw(a.x, b.x, left.x)
			&& betw(a.y, b.y, left.y)
			&& betw(c.x, d.x, left.x)
			&& betw(c.y, d.y, left.y);
	}
}


struct CBound
{
	int xl, yl, xr, yr, Len;
	bool IsLeft, IsRight;
};


class CDimmKeyPoints
{
private:

	void Approx(bool IsLeft, vector<CBound>& vBound, float& AngleMean, int Y, Point* L)
	{
		int mean[10000];
		::memset(mean, 0, sizeof(int) * 10000);

		int leftcnt = 0;
		int minval = 10000;
		int mind = 10000;
		int d;
		vector<int> vd;
		for (auto i : vBound)
		{
			if (i.Len > 1000)
			{
				if (IsLeft)
				{
					if (i.IsLeft)
					{
						int x = i.yl * AngleMean;
						d = x + i.xl;
						vd.push_back(d);
						leftcnt++;
						if (minval > i.xl)
							minval = i.xl;
						if (mind > d)
							mind = d;
					}
				}
				else
				{
					if (i.IsRight)
					{
						int x = i.yr * AngleMean;
						d = x + i.xr;
						vd.push_back(d);
						leftcnt++;
						if (minval > i.xr)
							minval = i.xr;
						if (mind > d)
							mind = d;
					}
				}
			}

		}
		for (auto i : vd)
		{
			mean[int(i - mind)] ++;
		}
		int i = 0, sum = 0;
		while ((i < 10000) && (sum < (leftcnt / 2)))
		{
			sum += mean[i++];
		}
		i--;

		L[0] = Point(i + mind, 0);
		L[1] = Point(i + mind - Y * AngleMean, Y);

	}

public:
	void find_keypoints(Mat& src, Point* key_point, int slot_qty = 100, bool is_rotated = false)
	{
		bool top_cpu_exist, bottom_cpu_exist, top_cpu_empty, bottom_cpu_empty = false;
		top_cpu_exist = bottom_cpu_exist = top_cpu_empty = bottom_cpu_empty = false;
		ExistingCpu(src, top_cpu_exist, bottom_cpu_exist);
		if (!top_cpu_exist && !bottom_cpu_exist)
			EmptyCpu(src, top_cpu_empty, bottom_cpu_empty);
		

		Ptr<CLAHE> clahe = createCLAHE();
		clahe->setClipLimit(20);
		Size size;
		size.height = 200;
		size.width = 200;
		clahe->setTilesGridSize(size);
		Mat cla;
		clahe->apply(src, cla);

		//imwrite("c:/tmp/bin.png", bin);

		Mat ker = Mat::ones(15, 27, CV_32F);

		//float base[5] = { -50,25,50,25,-50 };
		float kernel[15][27];

		for (int x = 0; x < 27; x++)
		{
			for (int y = 0; y < 15; y++)
			{
				kernel[y][x] = 0.33;
			}
		}

		for (int x = 7; x < 20; x++)
		{
			for (int y = 5; y < 15; y++)
			{
				kernel[y][x] = -0.66;
			}
		}

		float factor = (float)1 / 100;

		for (int x = 0; x < 27; x++)
		{
			for (int y = 0; y < 15; y++)
			{
				ker.at<float>(y, x) = kernel[y][x] * factor;

			}
		}

		/// Apply filter
		Mat con;
		filter2D(cla, con, -1, ker, Point(-1, -1), 0, BORDER_DEFAULT);

		double min, max;
		minMaxLoc(con, &min, &max);

		Mat bin2;
		threshold(con, bin2, 90, 256, THRESH_BINARY);

		Mat dil;
		Mat element = getStructuringElement(MORPH_RECT, Size(50, 1));
		/// Apply the dilation operation
		dilate(bin2, dil, element);


		vector<vector<Point> > contours;
		findContours(dil, contours, RETR_LIST, CHAIN_APPROX_SIMPLE);
		vector<RotatedRect> minRect(contours.size());
		for (int i = 0; i < contours.size(); i++)
			minRect[i] = minAreaRect(Mat(contours[i]));

		int Width, Length;
		float AngleMean = 0;
		int AngleCnt = 0;
		Point2f rect_points[4];
		vector<CBound> vBound;
		CBound Bound;
		float angle;
		int SlotMinLength = 1000;

		for (int i = 0; i < contours.size(); i++)
		{
			if (minRect[i].size.height > minRect[i].size.width)
			{
				Width = minRect[i].size.width;
				Length = minRect[i].size.height;
			}
			else
			{
				Length = minRect[i].size.width;
				Width = minRect[i].size.height;
			}

			int xmin, xidx, xmax, xmaxidx;
			if ((Length > SlotMinLength) && (Width < 100))
			{
				AngleCnt++;
				angle = minRect[i].angle;
				if (fabs(angle) > 45) angle += 90;
				AngleMean += angle;


				xmin = contours[i][0].x;
				xidx = 0;
				xmax = xmin;
				xmaxidx = xidx;
				for (int j = 1; j < contours[i].size(); j++)
				{
					if (contours[i][j].x < xmin)
					{
						xidx = j;
						xmin = contours[i][xidx].x;
					}
					if (contours[i][j].x > xmax)
					{
						xmaxidx = j;
						xmax = contours[i][xmaxidx].x;
					}

				}
				Bound.xl = contours[i][xidx].x;
				Bound.yl = contours[i][xidx].y;
				Bound.xr = contours[i][xmaxidx].x;
				Bound.yr = contours[i][xmaxidx].y;
				Bound.IsLeft = false;
				Bound.IsRight = false;
				Bound.Len = Length;
				vBound.push_back(Bound);

//drawContours(dst, contours, i, Scalar(0,0,255), 5);

/*minRect[i].points(rect_points);
for (int j = 0; j < 4; j++)
	line(dst, rect_points[j], rect_points[(j + 1) % 4], Scalar(255,0,0), 5, 8);*/

			}
		}


		AngleMean /= AngleCnt;
		float AngleDeg = AngleMean;
		float Cos = cos(AngleMean / 180 * 3.14159265359);
		AngleMean = sin(AngleMean / 180 * 3.14159265359);
		
		int MeanLeft = 0;
		for (auto i : vBound)
		{
			MeanLeft += (i.xl + i.xr) / 2;
		}
		bool LeftCrop = false, RightCrop = false;
		MeanLeft /= vBound.size();
		for (int i = 0; i < vBound.size(); i++)
		{
			if (vBound[i].xl < (MeanLeft - 300))
				vBound[i].IsLeft = true;
			if (vBound[i].xl <= 10)
				LeftCrop = true;
			if (vBound[i].xr > (MeanLeft + 300))
				vBound[i].IsRight = true;
			if (vBound[i].xr >= src.cols -10)
				RightCrop = true;
		}

		Point L[2], R[2], T[2], B[2];

		Approx(true, vBound, AngleMean, src.rows - 1, L);
		Approx(false, vBound, AngleMean, src.rows - 1, R);

		int SlotLength = (R[0].x - L[0].x) * Cos;

		// outer_from total_from
		int SlotHeight = SlotLength * 0.073;
		int SlotTotalHeight = slot_qty * SlotHeight;

		int miny = 10000, maxy = -1, minidx = -1, maxidx = -1;
		for (int i = 0; i < vBound.size(); i++)
		{
			if ((miny > vBound[i].yl) || (miny > vBound[i].yr))
			{
				minidx = i;
				miny = vBound[i].yl;
				if (vBound[i].yr < miny)
					miny = vBound[i].yr;
			}
			if ((maxy < vBound[i].yl) || (maxy < vBound[i].yr))
			{
				maxidx = i;
				maxy = vBound[i].yl;
				if (vBound[i].yr > maxy)
					maxy = vBound[i].yr;
			}
		}

		vector<CBound> vBound2;
		for (auto i : vBound)
		{
			if (!is_rotated)
			{
				if (i.yl < SlotTotalHeight + miny)
					vBound2.push_back(i);
			}
			else
			{
				if (i.yl > maxy - SlotTotalHeight)
					vBound2.push_back(i);
			}
		}
		vBound = vBound2;


/*for (int i = 0; i < vBound.size(); i++)
{
	line(dst, Point(vBound[i].xl, vBound[i].yl), Point(vBound[i].xr, vBound[i].yl), Scalar(0, 0, 255), 5);
	line(dst, Point(vBound[i].xr, vBound[i].yl), Point(vBound[i].xr, vBound[i].yr), Scalar(0, 0, 255), 5);
	line(dst, Point(vBound[i].xr, vBound[i].yr), Point(vBound[i].xl, vBound[i].yr), Scalar(0, 0, 255), 5);
	line(dst, Point(vBound[i].xl, vBound[i].yr), Point(vBound[i].xl, vBound[i].yl), Scalar(0, 0, 255), 5);
}*/

		////////////////////////////////

		Mat rot;
		Point2f pc(src.cols / 2., src.rows / 2.);
		cv::Mat RotMat = getRotationMatrix2D(pc, AngleDeg, 1.0);
		cv::warpAffine(dil, rot, RotMat, src.size());

		vector<vector<Point> > contours_;
		findContours(rot, contours_, RETR_LIST, CHAIN_APPROX_SIMPLE);
		vector<RotatedRect> minRect_(contours_.size());
		for (int i = 0; i < contours_.size(); i++)
			minRect_[i] = minAreaRect(Mat(contours_[i]));

		int xc = 0, qty = 0;
		for (int i = 0; i < minRect_.size(); i++)
		{
			if ((minRect_[i].size.height > 800)
				|| (minRect_[i].size.width > 800))
			{
				xc += minRect_[i].center.x;
				qty++;
			}
		}
		xc /= qty;

		struct CSlot
		{
			RotatedRect* rect;
			int pos;
			int before, after;
			int is_upper, is_lower;

			bool operator< (const CSlot & p) const {
				return rect->center.y < p.rect->center.y;
			}
		};

		CSlot slot;
		slot.before = 0;
		slot.after = 0;
		slot.is_lower = 0;
		slot.is_upper = 0;
		vector<CSlot> lslot, rslot;

		vector<int> lc, rc;
		for (int i = 0; i < minRect_.size(); i++)
		{
			if ((minRect_[i].size.height > SlotMinLength)
				|| (minRect_[i].size.width > SlotMinLength))
			{
				slot.rect = &minRect_[i];
				if (xc < 3000)
				{
					if (minRect_[i].center.x < xc)
					{
						lslot.push_back(slot);
					}
					else
					{
						rslot.push_back(slot);
					}
				}
				else
				{
					lslot.push_back(slot);
				}
			}
		}
		sort(lslot.begin(), lslot.end());
		sort(rslot.begin(), rslot.end());
		if (lslot.size() > 2)
		{
			for (int i = 1; i < lslot.size() - 1; i++)
			{
				lslot[i].before = lslot[i].rect->center.y - lslot[i - 1].rect->center.y;
				lslot[i].after = lslot[i + 1].rect->center.y - lslot[i].rect->center.y;
			}
		}
		if (rslot.size() > 2)
		{
			for (int i = 1; i < rslot.size() - 1; i++)
			{
				rslot[i].before = rslot[i].rect->center.y - rslot[i - 1].rect->center.y;
				rslot[i].after = rslot[i + 1].rect->center.y - rslot[i].rect->center.y;
			}
		}




		if (LeftCrop || RightCrop)
		{
			int a1 = 0, a2 = 0, a1q = 0, a2q = 0;
			for (int i = 0; i < lslot.size(); i++)
			{
				if ((lslot[i].after >= 110) && (lslot[i].after <= 145))
				{
					a1 += lslot[i].after;
					a1q++;
				}
				if ((lslot[i].after >= 205) && (lslot[i].after <= 240))
				{
					a2 += lslot[i].after;
					a2q++;
				}
			}
			if (a1q > 1)
			{
				a1 /= a1q;
				SlotHeight = a1 / 0.57;
				SlotLength = SlotHeight / 0.073;
			}
			if (a2q > 1)
			{
				a2 /= a2q;
				SlotHeight = a2;
				SlotLength = SlotHeight / 0.073;
			}
			R[0].x = L[0].x + Cos * SlotLength;
			R[1].x = L[1].x + Cos * SlotLength;
		}


		int outer = SlotHeight * 0.57;
		int inner = SlotHeight * 0.43;
		int offset_ = 15;
		int outer_from = outer - offset_;
		int outer_till = outer + offset_;
		int inner_from = inner - offset_;
		int inner_till = inner + offset_;
		int total_from = inner + outer - offset_;
		int total_till = inner + outer + offset_;


		vector<CSlot> buffer_slot;
		for (int i = 0; i < lslot.size(); i++)
		{
			if (!((lslot[i].after + lslot[i].before > inner_from) && (lslot[i].after + lslot[i].before < inner_till)))
				buffer_slot.push_back(lslot[i]);
		}
		lslot = buffer_slot;

		buffer_slot.clear();
		for (int i = 0; i < rslot.size(); i++)
		{
			if (!((rslot[i].after + rslot[i].before > inner_from) && (rslot[i].after + rslot[i].before < inner_till)))
				buffer_slot.push_back(rslot[i]);
		}
		rslot = buffer_slot;

		if (lslot.size() > 2)
		{
			for (int i = 1; i < lslot.size() - 1; i++)
			{
				lslot[i].before = lslot[i].rect->center.y - lslot[i - 1].rect->center.y;
				lslot[i].after = lslot[i + 1].rect->center.y - lslot[i].rect->center.y;
			}
		}
		if (rslot.size() > 2)
		{
			for (int i = 1; i < rslot.size() - 1; i++)
			{
				rslot[i].before = rslot[i].rect->center.y - rslot[i - 1].rect->center.y;
				rslot[i].after = rslot[i + 1].rect->center.y - rslot[i].rect->center.y;
			}
		}

		bool inverse = false;
		if (!top_cpu_empty && !bottom_cpu_empty)
		{
			inverse = true;
		}
		if (top_cpu_empty || top_cpu_exist)
		{
			inverse = false;
		}
		if (bottom_cpu_empty || bottom_cpu_exist)
		{
			inverse = true;
		}
		if ((lslot[0].rect->center.y < 300) || (is_rotated))
			inverse = true;

		vector<CSlot>& pslot = lslot;
		if (rslot.size() > 2)
		{
			if (inverse)
			{
				if (lslot[lslot.size()-1].rect->center.y < rslot[rslot.size()-1].rect->center.y)
					pslot = rslot;
			}
			else
			{
				if (lslot[0].rect->center.y > rslot[0].rect->center.y)
					pslot = rslot;
			}
		}

		bool IsUpper;
		bool found = false;
		if (inverse)
		{
			IsUpper = true;
			int in = pslot.size();
			int i = pslot.size() - 1;
			while (i >=0 && !found)
			{
				if ((pslot[i].before >= outer_from) && (pslot[i].before <= outer_till))
				{
					found = true;
					int d = pslot[in - 1].rect->center.y - pslot[i].rect->center.y;
					d %= SlotHeight;
					if ((d > inner_from) && (d < SlotHeight - inner_from))
						IsUpper = false;
				}
				i--;
			}
		}
		else
		{
			IsUpper = false;
			int i = 0;
			while (i < pslot.size() && !found)
			{
				if ((pslot[i].after >= outer_from) && (pslot[i].after <= outer_till))
				{
					found = true;
					int d = pslot[i].rect->center.y - pslot[0].rect->center.y;
					d %= SlotHeight;
					if ((d > inner_from) && (d < SlotHeight - inner_from))
						IsUpper = true;
				}
				i++;
			}
		}

		/*Mat qq;
		qq = dil.clone();
		qq.setTo(0);
		for(int i=pslot.size()-3; i< pslot.size();i++)
		line(dst, Point(0, pslot[i].rect->center.y), Point(500, pslot[i].rect->center.y), Scalar(0, 255, 0), 4);
		resize(qq, qq, Size(), .25, .25);
		imshow("qq", qq);*/

		//////////////////////////////////////////////////////

		if (inverse)
		{
			B[0].x = vBound[maxidx].xl;
			B[0].y = vBound[maxidx].yl;
			B[1].x = vBound[maxidx].xr;
			if (RightCrop)
				B[1].x = 7000;
			B[1].y = B[0].y + ((B[1].x - B[0].x) * AngleMean);
			


//line(dst, Point(B[0].x, B[0].y), Point(B[1].x, B[1].y), Scalar(0,0,255), 10);



			float tk = float(B[1].y - B[0].y) / (B[1].x - B[0].x);
			int yl = B[0].y - (tk * B[0].x);
			int yr = B[0].y + (tk * (src.cols - 1 - B[0].x));
			if (RightCrop)
				yr = B[0].y + (tk * (7000 - B[0].x));
			B[0].x = 0;
			B[0].y = yl;
			B[1].x = src.cols - 1;
			if (RightCrop)
				B[1].x = 7000;
			B[1].y = yr;

			if (!IsUpper)
			{
				B[0].y -= inner * Cos;
				B[1].y -= inner * Cos;
			}

			T[0] = B[0];
			T[1] = B[1];
			T[0].y -= SlotHeight * 4;
			T[1].y -= SlotHeight * 4;
		}
		else
		{
			T[0].x = vBound[minidx].xl;
			T[0].y = vBound[minidx].yl;
			T[1].x = vBound[minidx].xr;
			if (RightCrop)
				T[1].x = 7000;
			T[1].y = T[0].y + ((T[1].x - T[0].x) * AngleMean);

			float tk = float(T[1].y - T[0].y) / (T[1].x - T[0].x);
			int yl = T[0].y - (tk * T[0].x);
			int yr = T[0].y + (tk * (src.cols - 1 - T[0].x));
			if (RightCrop)
				yr = T[0].y + (tk * (7000 - T[0].x));
			T[0].x = 0;
			T[0].y = yl;
			T[1].x = src.cols - 1;
			if (RightCrop)
				T[1].x = 7000;
			T[1].y = yr;

			if (IsUpper)
			{
				T[0].y += inner * Cos;
				T[1].y += inner * Cos;
			}

			B[0] = T[0];
			B[1] = T[1];
			B[0].y += SlotHeight * 4;
			B[1].y += SlotHeight * 4;
		}

		/*if (is_rotated)
		{
			L[0].x = R[0].x - 1000;
			L[1].x = R[1].x - 1000;
		}
		else
		{
			R[0].x = L[0].x + 1000;
			R[1].x = L[1].x + 1000;
		}*/

		pt l[2], t[2], r[2], b[2], tmp, lt, lb, rt, rb;
		l[0].x = L[0].x; l[0].y = L[0].y;
		l[1].x = L[1].x; l[1].y = L[1].y;
		t[0].x = T[0].x; t[0].y = T[0].y;
		t[1].x = T[1].x; t[1].y = T[1].y;
		r[0].x = R[0].x; r[0].y = R[0].y;
		r[1].x = R[1].x; r[1].y = R[1].y;
		b[0].x = B[0].x; b[0].y = B[0].y;
		b[1].x = B[1].x; b[1].y = B[1].y;
		intersect(l[0], l[1], t[0], t[1], lt, tmp);
		intersect(l[0], l[1], b[0], b[1], lb, tmp);
		intersect(r[0], r[1], t[0], t[1], rt, tmp);
		intersect(r[0], r[1], b[0], b[1], rb, tmp);

		

		int offset = 5;
		lt.y += offset;
		rt.y += offset;
		lb.y += offset;
		rb.y += offset;

		int Bnd = 200;
		int Lbound = Bnd;
		int Rbound = src.cols - Bnd;
		int Tbound = Bnd;
		int Bbound = src.rows - Bnd;
		bool ActualL, ActualR, ActualT, ActualB;
		ActualL = ActualR = ActualT = ActualB = true;
		if ((L[0].x < Lbound) || (L[0].x > Rbound) || (L[1].x < Lbound) || (L[1].x > Rbound))
		{
			ActualL = false;
		}
		if ((R[0].x < Lbound) || (R[0].x > Rbound) || (R[1].x < Lbound) || (R[1].x > Rbound))
		{
			ActualR = false;
		}
		if ((T[0].y < Tbound) || (T[0].y > Bbound) || (T[1].y < Tbound) || (T[1].y > Bbound))
		{
			ActualT = false;
		}
		if ((B[0].y < Tbound) || (B[0].y > Bbound) || (B[1].y < Tbound) || (B[1].y > Bbound))
		{
			ActualB = false;
		}

		if (is_rotated || inverse)
		{
			key_point[2] = Point(lt.x, lt.y);
			key_point[3] = Point(rt.x, rt.y);
			key_point[0] = Point(rb.x, rb.y);
			key_point[1] = Point(lb.x, lb.y);
		}
		else
		{
			key_point[0] = Point(lt.x, lt.y);
			key_point[1] = Point(rt.x, rt.y);
			key_point[2] = Point(rb.x, rb.y);
			key_point[3] = Point(lb.x, lb.y);
		}



		//resize(dil, dil, Size(), .25, .25);
		//imshow("dil", dil);
	}

	void match_keypoints(Point* left, Point* right, pair<Point, Point>* match_keypoint)
	{
		pair<Point, Point> m;
		for (int i = 0; i < 4; i++)
		{
			match_keypoint[i].first = left[i];
			match_keypoint[i].second = right[i];
		}
	}

	struct CMsd
	{
		float msd;
		float k;
		float factor;
		int idx;
		int y;
	};

	void ExistingCpu(Mat& src, bool& on_top, bool& on_bottom)
	{
		on_top = on_bottom = false;

		Ptr<CLAHE> clahe = createCLAHE();
		clahe->setClipLimit(10);
		Size size;
		size.height = 150;
		size.width = 150;
		clahe->setTilesGridSize(size);
		Mat cla;
		clahe->apply(src, cla);

		Mat bin;
		adaptiveThreshold(cla, bin, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 101, 0);




		double area, len, rectarea;
		Rect rect;
		vector<vector<Point> > contours;
		findContours(bin, contours, RETR_LIST, CHAIN_APPROX_NONE, Point(0, 0));

		CBinary bin_contour, chord_contour;
		ev e;
		float dArg, totalArg;
		int counter;
		float norm, arg;
		float norm_from, norm_till, arg_from, arg_till, factor_from, factor_till;
		norm_from = 10;
		norm_till = 80;
		arg_from = 0.1;
		arg_till = 0.7;
		factor_from = 90;
		factor_till = 280;
		counter = 0;
		bool in_count;
		const int segment_number = 5;
		int total_len = 200;
		int segment_len = total_len / segment_number;
		ev chord;
		vector<Point> chord0;
		Point screen_point;
		Point contour0;
		int top_counter = 0;
		int bottom_counter = 0;

		for (int contour_no = 0; contour_no < contours.size(); contour_no++)
		//for (int contour_no = 82650; contour_no < 82700; contour_no++)
		//int contour_no = 82682;
		{
			if ((contours[contour_no].size() > 400))
			{

//drawContours(dst, contours, contour_no, Scalar(0, 0, 255), 10, 8, vector<Vec4i>(), 0, Point());

				vector<Point>& ref_contour = contours[contour_no];
				// 44336

				chord0.clear();
				bin_contour.mData.clear();
				chord_contour.mData.clear();
				contour0 = ref_contour[0];
				for (int j = 0; j < ref_contour.size() - 1; j++)
				{
					e.x = ref_contour[j + 1].x - ref_contour[j].x;
					e.y = ref_contour[j + 1].y - ref_contour[j].y;
					bin_contour.mData.push_back(e);
				}
				e.x = ref_contour[0].x - ref_contour[ref_contour.size() - 1].x;
				e.y = ref_contour[0].y - ref_contour[ref_contour.size() - 1].y;
				bin_contour.mData.push_back(e);

				screen_point = contour0;

				bin_contour.mData.insert(bin_contour.mData.end(), bin_contour.mData.begin(), bin_contour.mData.begin() + total_len);

				for (int offset = 0; offset < bin_contour.mData.size() - segment_len; offset += segment_len)
				{
					chord.x = 0;
					chord.y = 0;
					for (int i = offset; i < segment_len + offset; i++)
						chord += bin_contour.mData[i];
					chord_contour.mData.push_back(chord);

					screen_point.x += chord.x;
					screen_point.y += chord.y;
					chord0.push_back(screen_point);

				}


				float rotation = 0, prior_rotation = 0;
				bool reverse_rotation;
				in_count = false;
				reverse_rotation = false;
				totalArg = 0;
				ev start_direction, end_direction;

				start_direction.x = start_direction.y = end_direction.x = end_direction.y = 0;
				int i = 0;
				counter = 0;
				float Rotation = 0;
				float MaxRotation = 0;
				while (i < chord_contour.mData.size())
				{

					if (i < chord_contour.mData.size() - 1)
					{
						reverse_rotation = false;
						rotation = chord_contour.mData[i + 1].Arg() - chord_contour.mData[i].Arg();
						if (rotation < -pi)
							rotation += 2 * pi;
						if (rotation > pi)
							rotation -= 2 * pi;

						if (fabs(rotation) > 0.2)
						{
							if (((rotation > 0 && prior_rotation < 0) || (rotation < 0 && prior_rotation > 0)) && rotation && prior_rotation)
								reverse_rotation = true;
							else
								reverse_rotation = false;
							prior_rotation = rotation;
						}

						if (!reverse_rotation)
						{
							if (start_direction.x == start_direction.y && start_direction.x  == 0)
							{
								start_direction.x += chord_contour.mData[i].x;
								start_direction.y += chord_contour.mData[i].y;
								end_direction = start_direction;
							}
							end_direction.x += chord_contour.mData[i+1].x;
							end_direction.y += chord_contour.mData[i+1].y;
							totalArg += rotation;
							dArg = fabs(rotation);
						}
					}
					else
					{
						/*totalArg += chord_contour.mData[0].Arg() - chord_contour.mData[i].Arg();
						dArg = fabs(chord_contour.mData[0].Arg() - chord_contour.mData[i].Arg());
						if (dArg > pi)
							dArg = pi * 2 - dArg;*/
					}


					float z1 = chord_contour.mData[i].Norm();
					float z2 = start_direction.Arg();
					float z3 = end_direction.Arg();
					float z4 = fabs(z2-z3);

					Rotation = z4;
					if (MaxRotation < Rotation)
						MaxRotation = Rotation;

					if ((chord_contour.mData[i].Norm() > norm_from) && (chord_contour.mData[i].Norm() < norm_till)
						&& (dArg > arg_from) && (dArg < arg_till)
						&& !reverse_rotation
						//&& (chord_contour.mData[i].Norm() / dArg > factor_from) && (chord_contour.mData[i].Norm() / dArg < factor_till)
						)
					{
						in_count = true;
						counter++;
					}
					else
					{
						if (counter >= segment_number && fabs(MaxRotation) > 0.0)
						{

							vector<Point> arc;
							vector<Point>::const_iterator i1, i2;
							i1 = chord0.begin() + i - counter + 1;
							i2 = chord0.begin() + i + 1;
							arc.insert(arc.begin(), i1, i2);
							RotatedRect minRect = minAreaRect(Mat(arc));
							if (minRect.size.width > 20 && minRect.size.height > 20)
							{
								for (int j = i - counter + 1; j < i; j++)
								{

//line(dst, chord0[j], chord0[j + 1], Scalar(0, 0, 255), 5);

									if (chord0[j].y > src.rows / 2)
										bottom_counter++;
									else
										top_counter++;
								}
							}
						}
						in_count = false;
						counter = 0;
						totalArg = 0;
						MaxRotation = 0;
						start_direction.x = start_direction.y = end_direction.x = end_direction.y = 0;
					}
					i++;
				}
				if (counter >= segment_number && fabs(MaxRotation) > 0.0)
				{
					vector<Point> arc;
					vector<Point>::const_iterator i1, i2;
					i1 = chord0.begin() + i - counter + 1;
					i2 = chord0.begin() + i;
					arc.insert(arc.begin(), i1, i2);
					RotatedRect minRect = minAreaRect(Mat(arc));
					if(minRect.size.width > 20 && minRect.size.height > 20)
					{
						for (int j = chord_contour.mData.size() - counter; j < chord_contour.mData.size()-1; j++)
						{

//line(dst, chord0[j], chord0[j + 1], Scalar(0, 0, 255), 5);


							if (chord0[j].y > src.rows / 2)
								bottom_counter++;
							else
								top_counter++;
						}
					}
				}
			}
		}

		if (top_counter > bottom_counter && top_counter)
		{
			on_top = true;
			on_bottom = false;
		}
		else
		if(bottom_counter)
		{
			on_top = false;
			on_bottom = true;
		}

/*resize(cla, cla, Size(), .25, .25);
imshow("cla", cla);

resize(bin, bin, Size(), .25, .25);
imshow("bin", bin);*/
	}


	void EmptyCpu(Mat& src, bool& on_top, bool& on_bottom)
	{
		on_top = on_bottom = false;

		Ptr<CLAHE> clahe = createCLAHE();
		clahe->setClipLimit(10);
		Size size;
		size.height = 150;
		size.width = 150;
		clahe->setTilesGridSize(size);
		Mat cla;
		clahe->apply(src, cla);

		Mat bin;
		adaptiveThreshold(cla, bin, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 101, 50);

		double area, len, rectarea;
		Rect rect;
		vector<vector<Point> > contours;
		findContours(bin, contours, RETR_LIST, CHAIN_APPROX_NONE, Point(0, 0));

		CBinary bin_contour, chord_contour;
		ev e;
		float dArg, totalArg;
		int counter;
		float norm, arg;
		float norm_from, norm_till, arg_from, arg_till, factor_from, factor_till;
		norm_from = 30;
		norm_till = 100;
		arg_from = 0.1;
		arg_till = 0.7;
		factor_from = 90;
		factor_till = 280;
		counter = 0;
		bool in_count;
		const int segment_number = 5;
		int total_len = 400;
		int segment_len = total_len / segment_number;
		ev chord;
		vector<Point> chord0;
		Point screen_point;
		Point contour0;

		for (int contour_no = 0; contour_no < contours.size(); contour_no++)
		{
			if ((contours[contour_no].size() > 1000))
			{
				vector<Point>& ref_contour = contours[contour_no];

				chord0.clear();
				bin_contour.mData.clear();
				chord_contour.mData.clear();
				contour0 = ref_contour[0];
				for (int j = 0; j < ref_contour.size() - 1; j++)
				{
					e.x = ref_contour[j + 1].x - ref_contour[j].x;
					e.y = ref_contour[j + 1].y - ref_contour[j].y;
					bin_contour.mData.push_back(e);
				}
				e.x = ref_contour[0].x - ref_contour[ref_contour.size() - 1].x;
				e.y = ref_contour[0].y - ref_contour[ref_contour.size() - 1].y;
				bin_contour.mData.push_back(e);

				screen_point = contour0;

				bin_contour.mData.insert(bin_contour.mData.end(), bin_contour.mData.begin(), bin_contour.mData.begin() + total_len);

				for (int offset = 0; offset < bin_contour.mData.size() - segment_len; offset += segment_len)
				{
					chord.x = 0;
					chord.y = 0;
					for (int i = offset; i < segment_len + offset; i++)
						chord += bin_contour.mData[i];
					chord_contour.mData.push_back(chord);

					screen_point.x += chord.x;
					screen_point.y += chord.y;
					chord0.push_back(screen_point);

				}


				float rotation = 0, prior_rotation = 0;
				bool reverse_rotation;
				in_count = false;
				reverse_rotation = false;
				totalArg = 0;
				counter = 0;

				int i = 0;
				while (i < chord_contour.mData.size())
				{
					if (i < chord_contour.mData.size() - 1)
					{
						reverse_rotation = false;
						rotation = chord_contour.mData[i + 1].Arg() - chord_contour.mData[i].Arg();
						if (rotation < -pi)
							rotation += 2 * pi;
						if (rotation > pi)
							rotation -= 2 * pi;

						if (fabs(rotation) > 0.2)
						{
							if (((rotation > 0 && prior_rotation < 0) || (rotation < 0 && prior_rotation > 0) ) && rotation && prior_rotation)
								reverse_rotation = true;
							else
								reverse_rotation = false;
							prior_rotation = rotation;
						}

						if (!reverse_rotation)
						{
							totalArg += rotation;
							dArg = fabs(rotation);
						}
					}
					else
					{
						totalArg += chord_contour.mData[0].Arg() - chord_contour.mData[i].Arg();
						dArg = fabs(chord_contour.mData[0].Arg() - chord_contour.mData[i].Arg());
						if (dArg > pi)
							dArg = pi * 2 - dArg;
					}

					if ((chord_contour.mData[i].Norm() > norm_from) && (chord_contour.mData[i].Norm() < norm_till)
						&& (dArg > arg_from) && (dArg < arg_till)
						&& !reverse_rotation
						)
					{
						in_count = true;
						counter++;
					}
					else
					{
						if ((counter >= segment_number) && (fabs(totalArg) > 1.5) && (fabs(totalArg) < 2 * pi - .5))
						{
							if ((abs(chord0[i - counter + 1].x - chord0[i].x)) > 10)
							{
								for (int j = i - counter + 1; j < i; j++)
								{
//line(dst, chord0[j], chord0[j + 1], Scalar(0, 0, 255), 5);
									if (chord0[j].y > src.rows / 2)
										on_bottom = true;
									else
										on_top = true;
								}
							}
						}
						in_count = false;
						counter = 0;
						totalArg = 0;
					}
					i++;
				}
			}
		}
	}

};