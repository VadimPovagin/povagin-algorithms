#include "contour.h"

const double pi = 3.1415926535897932384626433832795;

CBinary operator-( CBinary& left, CBinary& right )
{
	CBinary tmp;
	ev unit;
	for ( unsigned long i = 0; i < left.mData.size(); i ++ )
	{
		unit.x = left.mData[i].x - right.mData[i].x;
		unit.y = left.mData[i].y - right.mData[i].y;
		tmp.mData.push_back( unit );
	}
	return tmp;
}

CBinary operator*( CBinary& left, double k )// ��������� �� �����
{
	CBinary tmp;
	tmp = left;
	for ( unsigned long i = 0; i < tmp.mData.size(); i ++ )
	{
		tmp.mData[i].x *= k;
		tmp.mData[i].y *= k;
	}
	return tmp;
}

CBinary operator*( CBinary& left, ev k )// ��������� �� ����� � ���������� ������������
{
	CBinary tmp;
	tmp = left;
	for ( unsigned long i = 0; i < tmp.mData.size(); i ++ )
	{
		tmp.mData[i].x = left.mData[i].x * k.x - left.mData[i].y * k.y;
		tmp.mData[i].y = left.mData[i].x * k.y + left.mData[i].y * k.x;
	}
	return tmp;
}

CBinary operator+( CBinary& left, CBinary& right )
{
	CBinary tmp;
	ev piece;

	for ( unsigned long i = 0; i < left.mData.size(); i ++ )
	{
		piece.x = left.mData[i].x + right.mData[i].x;
		piece.y = left.mData[i].y + right.mData[i].y;
		tmp.mData.push_back( piece );
	}
	return tmp;
}


void CBinary::Init( long k )
{
	mData.clear();
	mData.resize(k);
}

// ������� �� �����
CBinary CBinary::operator/(double k)
{
	for(unsigned long i = 0; i < mData.size(); i ++) mData[i] = mData[i] / k;
	return *this;
}

void CBinary::Capellini( CBinary& ss, CBinary& sa )
{
	CBinary ReverseContour(*this);
	ReverseContour.Reverse();
	ReverseContour.ComplexSopr();
	ss = (*this + ReverseContour) / 2;
	sa = (*this - ReverseContour) / 2;
}

void CBinary::Reverse()
{
	CBinary tmp;
	tmp.mData = mData;

	for(unsigned long i = 1; i < mData.size(); i ++)
	{
		tmp.mData[i].x = mData[mData.size() - i].x;
		tmp.mData[i].y = mData[mData.size() - i].y;
	}
	mData = tmp.mData;
}

void CBinary::ComplexSopr()
{
	for(unsigned long i = 0; i < mData.size(); i ++) mData[i].y = -mData[i].y;
}

void CBinary::Reflect( double Angle )
{
	Rotate( -2 * Angle );
	ComplexSopr();
}

void CBinary::d( long value )
{
	long index, arrsize;
	arrsize = mData.size();

	CBinary tmp = *this;
	for(unsigned long i = 0; i < mData.size(); i ++)
	{
		index = i + value;
		if(index >= arrsize) index -= arrsize;
		if(index < 0) index += arrsize;
		tmp.mData[i] = mData[index];
	}
	mData = tmp.mData;
}

void CBinary::Rotate( double Angle ) // ��������� ������ �� ���� Angle rad
{
	for(unsigned long i = 0; i < mData.size(); i ++) mData[i].Rotate( Angle );
}

double CBinary::Norm()
{
	double n = 0;
	for(unsigned long i = 0; i < mData.size(); i ++) n += mData[i].x * mData[i].x + mData[i].y * mData[i].y;
	return sqrt(n);
}

bool CBinary::operator==( CBinary& right )
{
	if ( !EqualOrder( right ) )
		return false;
	for(unsigned long i = 0; i < mData.size(); i ++)
	{
		if(mData[i] != right.mData[i]) return false;
	}
	return true;
}

void CBinary::Load( ev* arr, long size )
{
	ev p;
	mData.clear();
	for ( long i = 0; i < size - 1; i ++ )
	{
		p.x = arr[i + 1].x - arr[i].x;
		p.y = arr[i + 1].y - arr[i].y;
		mData.push_back( p );
	}
}
