#ifndef _Contour_h
#define _Contour_h

#include <windows.h>
#include "math.h"
#include <vector>
#include <map>
#include <float.h>

extern const double pi;
const double _2pi = 2.0 * pi;
const double pi2 = pi / 2.0;

struct ev
{
	double x, y;
	double a, n;		// ��� Rotate2

	ev()
	{
		x = y = 0.0;
	}

	ev(double l_x)
	{
		x = l_x;
		y = 0.0;
	}

	ev(double l_x, double l_y)
	{
		x = l_x;
		y = l_y;
	}

	ev operator*(const ev& right)
	{
		double tmp_x = x * right.x - y * right.y;
		y = x * right.y + right.x * y;
		x = tmp_x;
		return *this;
	}

	// �������� ������������ �����
	double Arg() { return atan2(y, x); }

	ev Rotate(double Angle)
	{
		double a = Arg() + Angle;
		const double n = Norm();

		x = n * cos(a);
		y = n * sin(a);

		return *this;
	}

	ev Rotate2(double Angle)
	{
		double aa = a + Angle;

		x = n * cos(aa);
		y = n * sin(aa);

		return *this;
	}

	void PrepareRotate2()
	{
		a = Arg();
		n = Norm();
	}

	bool operator!=(ev& right)
	{
		if(x != right.x || y != right.y) return true;
		return false;
	}

	bool operator==(ev& right)
	{
		if(x == right.x && y == right.y) return true;
		return false;
	}

	ev operator=(double value)
	{
		x = value;
		y = 0;
		return *this;
	};

	ev operator+(ev& right)
	{
		x = x + right.x;
		y = y + right.y;
		return *this;
	}

	ev operator+=(ev& right)
	{
		x += right.x;
		y += right.y;
		return *this;
	}

	// ������ ������������ �����
	double Norm() { return sqrt(x * x + y * y); };

	ev operator/(double value)
	{
		x /= value;
		y /= value;
		return *this;
	};
};

struct CElementary
{
	// ��������� ������������� �������:
	// ����� �����
	double mModule;

	// �������
	long mOrder;

	// ����� ������
	long mVertexNo;

	//���� ��������
	double mAngle;
	std::vector< ev > mData;

	void Init( double module, long k, long j )
	{
		ev piece(module);
		const double pij = 2 * pi * j;

		mData.clear();

		for ( long i = 0; i < k; i ++ )
		{
			mData.push_back( piece );
			piece.Rotate(pij / k );
		}
	}

	void Rotate( double Angle ) // ��������� ������ �� ���� Angle rad
	{
		for ( unsigned long i = 0; i < mData.size(); i ++ )
			mData[i].Rotate( Angle );
	}
};

class CBinary
{
	friend double ScalarB( CBinary& left, CBinary& right );
	friend ev ScalarC( CBinary& left, CBinary& right );
	friend double R( CBinary& left, CBinary& right );
	friend CBinary operator+( CBinary& left, CBinary& right );	// �����
	friend CBinary operator-( CBinary& left, CBinary& right );

public:

	std::vector< ev > mData;

	void Load(ev* arr, long size);
	bool operator==(CBinary& right);

	// ����� ������� � �������� / ����������� ������������
	double Norm(); 
	// ��������� ������ �� ���� Angle rad
	void Rotate(double Angle); 
	// �������� ��������� ����� �� �������
	void d(long value);
	//��������� ������������ ���, ������������ ���� Angle � ���� �������
	void Reflect(double Angle);
	// ������� � ���������� ������������ �������
	void ComplexSopr();
	// �������� ������
	void Reverse();
	// ���������� ���������
	void Capellini(CBinary& ss, CBinary& sa);
	// ������� �� �����
	CBinary operator/(double k);
	void Init(long k);

	CBinary operator=(const CElementary& right)
	{ 
		mData = right.mData; 
		return *this; 
	};

	bool EqualOrder(const CBinary& right) const
	{ 
		if(mData.size() == right.mData.size()) return true; 
		return false; 
	};

	CBinary(std::vector<ev> v) : mData(v){}
	CBinary(){};
};

#endif